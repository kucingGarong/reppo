//
//  YKViewRepo.m
//  Reppo
//
//  Created by Programmer Lucu on 7/8/13.
//  Copyright (c) 2013 Programmer 3. All rights reserved.
//

#import "YKViewRepo.h"

@interface YKViewRepo ()

@end

@implementation YKViewRepo

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
