//
//  YKAppDelegate.h
//  Reppo
//
//  Created by Programmer Lucu on 7/8/13.
//  Copyright (c) 2013 Programmer 3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
