//
//  main.m
//  Reppo
//
//  Created by Programmer Lucu on 7/8/13.
//  Copyright (c) 2013 Programmer 3. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YKAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YKAppDelegate class]));
    }
}
